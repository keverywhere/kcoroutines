package com.gitlab.denissov.kcoroutines

import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlin.concurrent.thread

fun main(args: Array<String>) {

    // threads without synchronization
    repeat(3) { id ->
        thread {
            doAction(id)
        }
    }

    Thread.sleep(1_000)

    // threads with synchronization
    repeat(3) { id ->
        thread {
            doSyncAction(id)
        }
    }

    Thread.sleep(1_000)

    // coroutines without synchronization
    runBlocking {
        repeat(3) { id ->
            launch {
                doSuspendAction(id)
            }
        }
    }

    Thread.sleep(1_000)

    // coroutines with synchronization
    runBlocking {
        repeat(3) { id ->
            launch {
                doSuspendSyncAction(id)
            }
        }
    }

    Thread.sleep(1_000)

    // coroutines with mutex
    runBlocking {
        repeat(3) { id ->
            launch {
                doSuspendMutexAction(id)
            }
        }
    }
}

private fun doAction(id: Int) {
    println("doAction Start $id")
    Thread.sleep(100)
    println("doAction End $id")
}

@Synchronized
private fun doSyncAction(id: Int) {
    println("doSyncAction Start $id")
    Thread.sleep(100)
    println("doSyncAction End $id")
}

private suspend fun doSuspendAction(id: Int) {
    println("doSuspendAction Start $id")
    delay(100)
    println("doSuspendAction End $id")
}

@Synchronized
private suspend fun doSuspendSyncAction(id: Int) {
    println("doSuspendSyncAction Start $id")
    delay(100)
    println("doSuspendSyncAction End $id")
}

val mutex = Mutex()

private suspend fun doSuspendMutexAction(id: Int) = mutex.withLock {
    println("doSuspendMutexAction Start $id")
    delay(100)
    println("doSuspendMutexAction End $id")
}
