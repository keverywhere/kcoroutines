package com.gitlab.denissov.kcoroutines

import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking

fun main() = runBlocking {
    val operationsCount = 0..5
    fixExecutionTime("synchronous") {
        operationsCount.forEach { index ->
            println(longTimeMethod("A", index))
            println(longTimeMethod("B", index))
        }
    }
    fixExecutionTime("asynchronous") {
        operationsCount.forEach { index ->
            val resultA = async { longTimeMethod("A", index) }
            val resultB = async { longTimeMethod("B", index) }
            awaitAll(resultA, resultB).forEach {
                println(it)
            }
        }
    }
    fixExecutionTime("asynchronous all") {
        val results = operationsCount.map { index ->
            listOf(
                async {
                    longTimeMethod("A", index)
                },
                async {
                    longTimeMethod("B", index)
                }
            )
        }.flatten()

        awaitAll(*results.toTypedArray()).forEach {
            println(it)
        }
    }
}

private suspend fun longTimeMethod(prefix: String, index: Int): String {
    // Simulate waiting for a network request to respond or performing complex work on the CPU
    delay(1_000)
    return shortTimeMethod(prefix, System.currentTimeMillis(), index)
}

private fun shortTimeMethod(prefix: String, time: Long, index: Int): String =
    "Call $prefix:$index returns result at $time ms"

private suspend fun fixExecutionTime(blockName: String, block: suspend () -> Unit) {
    val startTime = System.currentTimeMillis()
    println("start: $blockName")
    block()
    println("complete $blockName execution time: ${System.currentTimeMillis() - startTime} ms")
}