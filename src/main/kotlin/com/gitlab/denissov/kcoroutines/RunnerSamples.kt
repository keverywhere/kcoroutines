package com.gitlab.denissov.kcoroutines

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.newFixedThreadPoolContext
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext

fun main(args: Array<String>) = runBlocking<Unit> {

    val result = async {
        delay(1000)
        "resultValue"
    }
    println("after async")
    println(result.await())

    launch {
        delay(500)
        println("print A")

        launch {
            delay(1_000)
            println("print D")
        }
    }

    launch {
        delay(100)
        println("print B")
    }

    delay(200)
    println("print C")

    launch(newSingleThreadContext("Single thread")) {
        repeat(10) { index ->
            launch {
                println("${Thread.currentThread().name} $index ")
            }
        }
    }.join()

    launch(newFixedThreadPoolContext(2, "Thread pool")) {
        repeat(10) { index ->
            launch {
                println("${Thread.currentThread().name} $index ")
            }
        }
    }.join()

    launch {
        val requestData = withContext(Dispatchers.IO) { apiRequest() }
        val formattedData = withContext(Dispatchers.Default) { formatData(requestData) }
        launch (Dispatchers.Default) { updateUI(formattedData) }
    }
}

suspend fun apiRequest(): String {
    delay(1000)
    return "revres morf atad"
}

fun formatData(data: String): String {
    return data.map { it.toUpperCase() }.reversed().joinToString(separator = "")
}

fun updateUI(data: String) {
    println("data: $data")
}
